from filereader.cleaner import apply_elementwise, reduce_whitespaces, replace_symbols
from filereader.closer import write_table
from filereader.opener import FILE_EXTENSIONS, read_table

if __name__ == "__main__":
    from tkinter import filedialog as fd

    in_filename = fd.askopenfilename(
        title="Open Table",
        initialdir='~',
        filetypes=[
            ('Tables', '*.csv *.xls *.xlsx'),
            ('Any', '*.*')
        ]
    )
    print(f"Reading from {in_filename}.")

    data = read_table(in_filename)
    cleaned_data = apply_elementwise(
        data,
        function=lambda string: reduce_whitespaces(replace_symbols(string, symbols_to_keep=r'\w\s/?!;,:._-', replacing_with='')),
        ignore_columns=[]
    )

    out_filename = fd.asksaveasfilename(
        title="Save Table",
        filetypes=FILE_EXTENSIONS,
        defaultextension='.csv')

    print(f"Writing to {out_filename}.")
    write_table(cleaned_data, out_filename)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
