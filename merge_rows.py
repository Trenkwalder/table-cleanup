from filereader.cleaner import apply_elementwise, reduce_whitespaces, replace_symbols
from filereader.closer import write_table
from filereader.opener import FILE_EXTENSIONS, read_table

if __name__ == "__main__":
    from tkinter import filedialog as fd

    in_filename = fd.askopenfilename(
        title="Open Table",
        initialdir='~',
        filetypes=[
            ('Tables', '*.csv *.xls *.xlsx'),
            ('Any', '*.*')
        ]
    )
    print(f"Reading from {in_filename}.")

    data = read_table(in_filename)
    data.TID = data["TID"].apply(str)
    res = data.groupby('ShopID')['TID'].apply(lambda x: f'"{",".join(x)}"').reset_index()

    print(res)

    out_filename = fd.asksaveasfilename(
        title="Save Table",
        filetypes=FILE_EXTENSIONS,
        defaultextension='.csv')

    print(f"Writing to {out_filename}.")
    write_table(res, out_filename)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
