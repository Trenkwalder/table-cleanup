import pandas as pd
import pytest
from pandas.testing import assert_frame_equal

from filereader.opener import read_table


def test_test_xlsx_reading():
    data = pd.read_excel(r'test.xlsx')

    assert_frame_equal(data, pd.DataFrame({
        'aa': ["test", "scheiß"],
        'bb': [1234, "Öffnen"],
        'cc': ["https://trenkwalder.tech:1234", "µ-controller"]
    }))

def test_test_csv_reading():
    data = pd.read_csv(r'test.csv', encoding='iso-8859-3')

    expected = pd.DataFrame({
        'aa': ["test", "scheiß"],
        'bb': [1234, "Öffnen"],
        'cc': ["https://trenkwalder.tech:1234", "µ-controller"]
    })

    for column in data:
        for i in range(len(expected[column])):
            assert str(expected[column][i]) == str(data[column][i])


def test_test_xls_reading():

    data = pd.read_excel(r'test.xls')

    assert_frame_equal(data, pd.DataFrame({
        'aa': ["test", "scheiß"],
        'bb': [1234, "Öffnen"],
        'cc': ["https://trenkwalder.tech:1234", "µ-controller"]
    }))

@pytest.mark.parametrize(
    "filename",
    [
        'test.xlsx',
        'test.csv',
        'test.xls'
    ]
)
def test_file_can_be_read_independently_of_the_format(filename):
    data = read_table(filename)

    return data.equals(pd.DataFrame({
        'aa': ["test", "scheiß"],
        'bb': [1234, "Öffnen"],
        'cc': ["https://trenkwalder.tech:1234", "µ-controller"]
    })) or data.equals(pd.DataFrame({
        'aa': ["test", "scheiß"],
        'bb': ["1234", "Öffnen"],
        'cc': ["https://trenkwalder.tech:1234", "µ-controller"]
    }))

