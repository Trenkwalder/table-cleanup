import pytest

from filereader.cleaner import *


@pytest.mark.parametrize(
    "input,expected",
    [
        ("Das ist ein Teststring.", "Das ist ein Teststring."),
        ("url: https://trenkwalder.tech:12345/?variable=?", "url: https://trenkwalder.tech:12345/?variable=?"),
        ("98 Shirley Street, PIMPAMA QLD, 4209 AUSTRALIA", "98 Shirley Street, PIMPAMA QLD, 4209 AUSTRALIA"),
    ]
)
def test_ascii_stays_ascii(input, expected):
    assert ensure_ascii_encoding(input) == expected

@pytest.mark.parametrize(
    "input,expected",
    [
        ("Das ist ein Übungsstring.", "Das ist ein Ubungsstring."),
        ("Die Straße ist für Füße.", "Die Strasse ist fur Fusse."),
        ("Familia pequeña", "Familia pequena"),
        ("¿Cuál es el último paso?", "?Cual es el ultimo paso?"),
        ("Norge og hvorvidt nok blir gjort for å ivareta vårt kjære språk.",
         "Norge og hvorvidt nok blir gjort for a ivareta vart kjaere sprak."),
        ("Benim için önemli bir gün, çünkü bugün babamın da doğum günü. O da artık kırk yaşında.",
         "Benim icin onemli bir gun, cunku bugun babamin da dogum gunu. O da artik kirk yasinda."),
    ]
)
def test_special_characters_can_be_converted_to_ascii(input, expected):
    assert ensure_ascii_encoding(input) == expected

@pytest.mark.parametrize(
    "input,expected",
    [
        ("This string has normal spaces.", "This string has normal spaces."),
        ("This  string  has  too many spaces.    ", "This string has too many spaces."),
        ("This\tstring\t\thas\r \nother\t \t\t whitespaces.\r\n", "This string has other whitespaces."),
    ]
)
def test_whitespaces_are_replaced_by_single_space(input, expected):
    assert reduce_whitespaces(input) == expected

@pytest.mark.parametrize(
    "input,expected",
    [
        ("This string has normal spaces.", "This string has normal spaces."),
        (r"This\\ string #\*has \strange+~+ characters\}[&$§\\.\'", "This string has strange characters."),
    ]
)
def test_special_characters_can_be_replaced(input, expected):
    assert replace_symbols(input=input, symbols_to_keep=r'\w\s/?!;,:._-', replacing_with='') == expected

def test_each_element_in_a_table_can_be_cleaned():

    test_data = pandas.DataFrame({
        'aa': ["test", "scheiß"],
        'bb': ["Öffnen", "Füße"],
        'x': ["µ-code", "???"]
    })
    expected = pandas.DataFrame({
        'aa': ["test", "scheiss"],
        'bb': ["Offnen", "Fusse"],
        'x': ["u-code", "???"]
    })

    cleaned_data = clean_table(test_data)
    assert expected.equals(cleaned_data)
