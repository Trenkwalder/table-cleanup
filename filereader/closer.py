import pandas


def write_table(data: pandas.DataFrame, file_name: str):
    from pathlib import Path

    path = Path(file_name)
    extension = path.suffix

    if extension in (".xls", ".xlsx"):
        return data.to_excel(file_name) #, encoding='utf-8'
    elif extension == ".csv":
        return data.to_csv(file_name, encoding='utf-8')
    else:
        pass