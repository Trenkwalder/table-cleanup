import pandas

FILE_EXTENSIONS = [
    ("XML Excel File", "*.xlsx"),
    ("Excel File", "*.xls"),
    ("CSV", "*.csv"),
    ("Open Office Spreadsheet", "*.ods"),
    ("Any File", "*.*"),
]

def determine_file_properties(file_name: str):
    with open(file_name, 'rb') as f:
        data = f.read()

        encoding = determine_encoding(data)
        separator = determine_delimiter(data.decode(encoding=encoding))

        return encoding, separator
def determine_encoding(data: bytes):
    import chardet

    return chardet.detect(data)['encoding']

def determine_delimiter(data: str):
    import csv

    return csv.Sniffer().sniff(data).delimiter

def read_table(file_name: str) -> pandas.DataFrame:
    from pathlib import Path

    path = Path(file_name)
    extension = path.suffix

    if extension in (".xls", ".xlsx"):
        return pandas.read_excel(file_name)
    elif extension == ".csv":
        encoding, separator = determine_file_properties(file_name)
        return pandas.read_csv(file_name, sep=separator, encoding=encoding)
    else:
        pass


def get_input_table() -> str:
    from PySide6.QtWidgets import QMainWindow

    class MainWindow(QMainWindow):
        def __init__(self):
            super().__init__()
            self.init_gui()

        def init_gui(self):

            file_names = QFileDialog.getOpenFileNames(self, "Select one or more files to open", "C:/Windows", "")
            print(file_names)

    from PySide6.QtWidgets import QFileDialog

    main = MainWindow()
    return QFileDialog.getOpenFileName(
        parent=main,
        caption="Open Table",
        dir="~",
        filter="Tables (*.csv *.xls *.xlsx)"
    )