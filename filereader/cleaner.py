import pandas

def ensure_ascii_encoding(input: str) -> str:
    from unidecode import unidecode
    return unidecode(input)


def reduce_whitespaces(input: str) -> str:
    import re
    return re.sub(r"\s+", " ", input).rstrip(" ")


def replace_symbols(input: str, symbols_to_keep=r'\w\s/?!;,:._-', replacing_with='') -> str:
    import re
    return re.sub(f"[^{symbols_to_keep}]+", replacing_with, input)

def clean_table(data: pandas.DataFrame, ignore_columns=[]) -> pandas.DataFrame:
    return apply_elementwise(
        data,
        function=lambda string : reduce_whitespaces(replace_symbols(ensure_ascii_encoding(string))),
        ignore_columns=ignore_columns
    )
def apply_elementwise(data: pandas.DataFrame, function, ignore_columns=[]) -> pandas.DataFrame:
    for column in data:
        if column in ignore_columns:
            continue

        for i in range(len(data[column])):
            element = data[column][i]
            if isinstance(element, str) or isinstance(element, bytes):
                new_value = function(element)
                data[column][i] = new_value

    return data